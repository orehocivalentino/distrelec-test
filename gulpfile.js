"use strict";
var gulp = require("gulp");
var sass = require("gulp-sass");
var csso = require('gulp-csso');

gulp.task("sass", function () {
	return gulp.src("src/sass/*.scss")
	.pipe(sass().on("error", sass.logError))
    .pipe(csso())
	.pipe(gulp.dest("dist/css"));
});