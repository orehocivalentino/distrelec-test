$(document).ready(function(){
	$.get( "data.json", function( data ) {
		var body = $('body');
		var carousel = new Carousel(body, data.carouselData);
		carousel.init();
	});
});

function Carousel(container , data) {

	function renderCarousel() {
		var context = data;
		var source = container.find('.js-carouselTemplate').html();
		var template = Handlebars.compile(source);
		var html = template(context);

		var outputTemplate = container.find('.js-carouselContainer');
		outputTemplate.html(html);

	}

	function bindButtonControls() {

		var scrollAmount = 0;
		var productsContainer = container.find('.js-productsContainer');
		var upBtn = container.find('.js-carouselMoveUp');
		var downBtn = container.find('.js-carouselMoveDown');

        downBtn.click(function (e){

        	var scrollEnabled = scrollAmount + productsContainer.height() == productsContainer[0].scrollHeight;
			scrollEnabled ? scrollAmount : scrollAmount+= 170;

            productsContainer.stop().animate({
                scrollTop: scrollAmount
       		}, 300,'linear',function(){
            	upBtn.prop("disabled" , productsContainer.scrollTop() == 0);
            	downBtn.prop("disabled" , productsContainer.scrollTop() + productsContainer.height() == productsContainer[0].scrollHeight);
            });
		});

		upBtn.click(function (e){
			scrollAmount == 0 ? 0 : scrollAmount-= 170;
            productsContainer.stop().animate({
                scrollTop:scrollAmount == 0 ? 0 : scrollAmount
       		}, 300,'linear',function(){
            	upBtn.prop("disabled" , productsContainer.scrollTop() == 0);
            	downBtn.prop("disabled" , productsContainer.scrollTop() + productsContainer.height() == productsContainer[0].scrollHeight);
            });
		})
    }

	this.init = function() {
		renderCarousel();
		bindButtonControls();
	};
}